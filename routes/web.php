<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test',function () {
    return $permissions = App\Permission_Role::with('permission')->whereHas('permission',function($e){
        $e->where('parent',-1);
    })->get();
    $parentMenus = Illuminate\Support\Collection::make();
    $childMenus = Illuminate\Support\Collection::make();
    foreach($permissions as $permission){
        if($permission['permission']['parent'] == -1)
            $parentMenus->push($permission['permission']);
        else
            $childMenus->push($permission['permission']);
    }
    return compact('parentMenus','childMenus');
});

Auth::routes();
Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/login',function () {
    return view('auth.login');
})->name('login');

Route::get('/admin/dash', 'DashController@index')->name('dash');
Route::resource('/admin/user','UserController');
Route::resource('/admin/role','RoleController');
Route::put('/admin/permission/priority','PermissionController@updatePriority')->name('permission.priotity');
Route::resource('/admin/permission','PermissionController');
Route::resource('/admin/contract','ContractController');
Route::resource('/admin/client','ClientController');
Route::resource('/admin/stage','StageController');
Route::resource('/admin/payment','PaymentController');
Route::resource('/admin/record','RecordController');
Route::resource('/admin/seller','SellerController');
Route::resource('/admin/polygon','PolygonController');
Route::resource('/admin/terrain','TerrainController');
Route::resource('/admin/status','StatusController');
Route::resource('/admin/fund','FundController');
Route::resource('/admin/dummy', 'DummyController');
Route::get('/admin/contract/run','ContractController@index')->name('contract.run');
Route::get('/admin/contract/expired','ContractController@index')->name('contract.late');
Route::get('/admin/payment/month','ContractController@index')->name('payment.month');
Route::get('/admin/payment/late','ContractController@index')->name('payment.late');
Route::get('/admin/polygons/map','ContractController@index')->name('polygons.map');
