<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->double('price');
            $table->double('paid_amount');
            $table->date('sale_date');
            $table->date('first_payment');
            $table->double('pending_payment');
            $table->string('bank_reference');
            $table->double('special_payment');
            $table->double('amount_payment');
            $table->date('next_payment_date');
            $table->integer('seller_id');
            $table->integer('client_id');
            $table->integer('terrain_id');
            $table->integer('status_id');
            $table->integer('fund_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
