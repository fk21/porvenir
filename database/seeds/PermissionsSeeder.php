<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'route'=>'dash',
            'title'=>'Dashboard',
            'icon'=>'flaticon-dashboard',
            'menu'=>true,
            'priority'=> 1,
            'parent'=> -1
        ]);
        Permission::create([
            'route'=>'configuracion',
            'title'=>'Configuración',
            'icon'=>'',
            'menu'=>false,
            'priority'=> 2,
            'parent'=> -1
        ]);
        Permission::create([
            'route'=>'permission_roles',
            'title'=>'Roles y Permisos',
            'icon'=>'flaticon-users',
            'menu'=>true,
            'priority'=>3,
            'parent'=>-1
        ]);
        Permission::create([
            'route'=>'user.create',
            'title'=>'Usuarios',
            'icon'=>'',
            'menu'=>true,
            'priority'=>1,
            'parent'=>3
        ]);
        Permission::create([
            'route'=>'role.create',
            'title'=>'Roles',
            'icon'=>'',
            'menu'=>true,
            'priority'=>2,
            'parent'=>3
        ]);
        Permission::create([
            'route'=>'permission.create',
            'title'=>'Permisos',
            'icon'=>'',
            'menu'=>true,
            'priority'=>3,
            'parent'=>3
        ]);
    }
}
