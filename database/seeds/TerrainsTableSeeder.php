<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class TerrainsTableSeeder extends CsvSeeder
{
    public function __construct()
	{
		$this->table = 'terrains';
        $this->filename = base_path().'/database/seeds/csvs/terrains.csv';
        //$this->insert_chunk_size = 1;
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommende d when importing larger CSVs
		//DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        parent::run();
    }
}
