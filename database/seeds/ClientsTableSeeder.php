<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class ClientsTableSeeder extends CsvSeeder
{
    public function __construct()
	{
		$this->table = 'clients';
        $this->filename = base_path().'/database/seeds/csvs/clients.csv';
        //$this->insert_chunk_size = 1;
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommende d when importing larger CSVs
		//DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        parent::run();
    }
}
