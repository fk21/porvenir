<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Carbon\Carbon;


class UserController extends Controller
{
    
    function __construct() {
        $this->middleware('ajax')->only(['index','edit']);
        $this->middleware('auth');
    }
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $p = $this->getPermission('user.create');
        $data = $p->r ? User::with('role')->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>true,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $p = $this->getPermission('user.create');
        return view('user.create');
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //$request->input('password') = bcrypt($request->input('password'));
        return User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'role_id' => $request->input('role_id'),
            'password' => bcrypt($request->input('password')),
            'created_at' => Carbon::now(),
            'updated_at' =>Carbon::now()
        ]);
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $data = User::findOrFail($id);
        $roles = Role::all();
        $view = view('user.edit',['data'=>$data, 'roles'=>$roles])->render();
        return compact('view');
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        return (int)$user->update($request->all());
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        return (int)$user->delete();
    }
}
