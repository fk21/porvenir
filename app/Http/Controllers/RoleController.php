<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\Permission_Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = $this->getPermission('role.create');
        $data = $p->r ? Role::with('permissions')->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $perm = array("_token"=>$data['_token'], "name"=>$data['name'], "description"=>$data['description']);
        $id = Role::create($perm)->id;
        $r = $this->storeRolePermission($id, $data['_token'], $data['permisos']);
        return $data;
    }

    private function storeRolePermission($id, $token, $permisos)
    {
        $r = 1;
        foreach ($permisos as $key => $value) {
           if(isset($value['permit']) && $value['permit'] == "on"){
                $data = array(
                    "_token"=>$token, 
                    "permission_id"=>(int)$value['id'],
                    "role_id"=>$id,
                    "c"=>(int)($value['C'] == "true"),
                    "r"=>(int)($value['R'] == "true"),
                    "u"=>(int)($value['U'] == "true"),
                    "d"=>(int)($value['D'] == "true"),
                );
                $r = Permission_Role::create($data);
                $permission = Permission::find((int)$value['id']);
                $this->createParent($permission->parent, $token, $id);
           }
        }
        return $r;
    }

    private function createParent($id_permission, $token, $id_role)
    {
        if ($id_permission == -1) {
            return;
        }
        $permission = Permission::find($id_permission);
        if (Permission_Role::where([['permission_id', $permission->id], ['role_id', $id_role]])->first()) {
            return ;
        }
        $data = array(
            "_token"=>$token,
            "permission_id"=>$permission->id,
            "role_id"=>$id_role,
            "c"=>0,
            "r"=>0,
            "u"=>0,
            "d"=>0,
        );
        Permission_Role::create($data);
        $this->createParent($permission->parent, $token, $id_role);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $role = Role::where('id', $role->id)->with('permissions')->get();
        return $role[0]->permissions;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Role::findOrFail($id);
        $view = view('role.edit',['data'=>$data])->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $data = $request->all();
        Permission_Role::where('role_id', $role['id'])->delete();
        $r = $this->storeRolePermission($role['id'], $data['_token'], $data['permisos']);
        return (int)$role->update(array("_token"=>$data['_token'], "name"=>$data['name'], "description"=>$data['description']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        return (int)$role->delete();
    }
}
