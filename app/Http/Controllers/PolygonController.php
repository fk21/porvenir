<?php

namespace App\Http\Controllers;

use App\Terrain;
use App\Polygon;
use Illuminate\Http\Request;

class PolygonController extends Controller
{

    function __construct() {
        $this->middleware('ajax')->only(['index','edit','update','destroy']);
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = $this->getPermission('polygon.create');
        $data = $p->r ? Polygon::with('terrain')->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $p = $this->getPermission('polygon.create');
        return view('polygon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Polygon::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Polygon  $polygon
     * @return \Illuminate\Http\Response
     */
    public function show(Polygon $polygon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Polygon  $polygon
     * @return \Illuminate\Http\Response
     */
    public function edit(Polygon $polygon)
    {
        $terrains = Terrain::all();
        $view = view('polygon.edit',compact('polygon','terrains'))->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Polygon  $polygon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Polygon $polygon)
    {
        return (int)$polygon->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Polygon  $polygon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Polygon $polygon)
    {
        return (int)$polygon->delete();
    }
}
