<?php

namespace App\Http\Controllers;

use App\Dummy;
use Illuminate\Http\Request;
use App\Role;

class DummyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = $this->getPermission('dummy.create');
        $data = $p->r ? Dummy::with('role')->get() : [];
        foreach ($data as $d)
            $d['p'] = array('a' => false, 'e' => $p->u, 'd' => $p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dummy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Dummy::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dummy  $dummy
     * @return \Illuminate\Http\Response
     */
    public function show(Dummy $dummy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dummy  $dummy
     * @return \Illuminate\Http\Response
     */
    public function edit(Dummy $dummy)
    {
        $data = $dummy;
        $roles = Role::all();
        $view = view('dummy.edit', compact('data','roles'))->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dummy  $dummy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dummy $dummy)
    {
        return (int) $dummy->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dummy  $dummy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dummy $dummy)
    {
        return (int)$dummy->delete();
    }
}
