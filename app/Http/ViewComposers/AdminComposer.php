<?php

namespace App\Http\ViewComposers;

use Auth;
use App\User;
use App\Permission_Role;
use Illuminate\View\View;
use Illuminate\Support\Collection;

class AdminComposer
{
    protected $permissions;
    /**
    * Create a movie composer.
    *
    * @return void
    */
    public function __construct()
    {
        $permissions = Permission_Role::with(['permission'])
                                    ->where('role_id',Auth::user()->role_id)
                                    ->get();
        $parentMenus = Collection::make();
        $childMenus = Collection::make();
        foreach($permissions as $permission){
            if($permission['permission']['parent'] == -1)
                $parentMenus->push($permission['permission']);
            else
                $childMenus->push($permission['permission']);
        }
        $this->permissions = compact('parentMenus','childMenus');
    }
    
    /**
    * Bind data to the view.
    *
    * @param  View  $view
    * @return void
    */
    public function compose(View $view)
    {
        $view->with('permissions', $this->permissions);
    }
}