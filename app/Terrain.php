<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terrain extends Model
{
    protected $fillable = [
        'lot', 'block', 'area','lat','lng','stage_id','cadastrian_key','location',
        'land_use','status','price'
    ];

    public function stage(){
        return $this->belongsTo('App\Stage');
    }
}
