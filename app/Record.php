<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $fillable = [
        'client_it', 'action'
    ];

    public function client(){
        return $this->belongsTo('App\Client');
    }
}
