<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
        'number', 'price', 'paid_amount','sale_date','first_payment','pending_payment','bank_reference',
        'special_payment' ,'amount_payment','next_payment_date','seller_id','client_id','terrain_id','fund_id'
    ];

    public function seller(){
        return $this->belongsTo('App\Seller');
    }
    public function client(){
        return $this->belongsTo('App\Client');
    }
    public function terrain(){
        return $this->belongsTo('App\Terrain');
    }
    public function fund(){
        return $this->belongsTo('App\Fund');
    }
    public function status(){
        return $this->belongsTo('App\Status');
    }
}
