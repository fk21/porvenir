<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polygon extends Model
{
    protected $fillable = [
        'terrain_id', 'order', 'lat','lng'
    ];

    public function terrain(){
        return $this->belongsTo('App\Terrain');
    }
}
