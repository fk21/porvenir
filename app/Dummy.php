<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dummy extends Model
{
    protected $fillable = [
        'first_name', 'middle_name', 'role_id',
    ];


    public function role()
    {
        return $this->belongsTo(Role::class);
    }

}
