<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'contract_id', 'amount', 'limit_date','paid_date','type_pay','payment_number','status'
    ];
}
