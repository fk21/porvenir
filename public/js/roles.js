"use strict;"
var Roles = function() {
    var table;
    var mod = "role";

    function setEvents(el) {
        var d = new Date();
        d = '?v=' + d.getTime();
        el.find("#submitForm input[type='checkbox']").on('change', function() {
            var checked = $(this).prop('checked');
            $(this).parent().siblings('div').find("input[type='checkbox']").prop('checked', checked);
            $('#submitForm .access-panel [name*="permisos["]').each(updatePermisosUI);
        });
        el.find(".access-panel i").tooltip();
        $.get('/Mviews/permisos_partial.html' + d, function(template) {
            el.find("#submitForm .access-panel").popover({
                title: "Permisos",
                html: true,
                content: template
            });
            el.find("#submitForm .access-panel").on('show.bs.popover', function() {
                el.find("#submitForm .access-panel").popover('hide');
            });
            el.find("#submitForm .access-panel").on('shown.bs.popover', function() {
                var popover = $(this);
                var id_pop = popover.attr('aria-describedby');
                popoverPermisos(popover, $("#" + id_pop), el);
            });
        });
    }

    function popoverPermisos(that, pop, el) {
        pop.on('click', '.cancel', function() {
            that.popover('hide');
        });
        that.find("input[type='hidden']").each(function(key, value) {
            pop.find("[name='menu[" + $(value).attr("id") + "]']").prop('checked', $(value).val() === "true");
        });
        pop.on('click', '.confirm', function() {
            var permisos = pop.find('form').serializeObject();
            that.find("input[type='hidden']").val(false);
            that.siblings('.m-checkbox-parent').each(function() {
                $(this).find('.access-panel').each(function() {
                    var child = $(this).siblings('label').children('input[type="checkbox"]');
                    if (child.prop('checked') || child.prop('indeterminate'))
                        $(this).find("input[type='hidden']").val(false);
                });
            });
            for (key in permisos.menu) {
                that.siblings('.m-checkbox-parent').each(function() {
                    $(this).find('.access-panel').each(function() {
                        var child = $(this).siblings('label').children('input[type="checkbox"]');
                        if (child.prop('checked') || child.prop('indeterminate'))
                            $(this).find("#" + key).val(permisos.menu[key]);
                    });
                });
                that.find("#" + key).val(permisos.menu[key]);
            }
            el.find('#submitForm .access-panel [name*="permisos["]').each(updatePermisosUI);
            that.popover('hide');
        });
    }

    function updatePermisosUI() {
        var that = $(this);
        var id = that.attr("id");
        var val = that.val() === "true" ? true : false;
        var checkbox = that.closest('.access-panel').siblings('.m-checkbox').children('[type="checkbox"]');
        var siblings = that.closest('.access-panel').siblings('.m-checkbox-parent');
        var children = siblings.find('#' + id).length;
        var checked = siblings.find('#' + id + '[value="true"]').length;

        if (!checkbox.prop('checked') && !checkbox.prop('indeterminate')) {
            that.siblings("[data-for]").removeClass('display');
            that.val(false);
            return;
        }

        if (children > 0 && checked === 0) {
            that.val(false);
            val = false;
        }

        if (val) {
            that.siblings("[data-for='NaN']").removeClass('display');
            that.siblings("[data-for='" + id + "']").addClass('display');
            if (children > 0 && checked != children)
                that.siblings("[data-for='" + id + "']").addClass('partial');
            else
                that.siblings("[data-for='" + id + "']").removeClass('partial');
        } else {
            that.siblings("[data-for='" + id + "']").removeClass('display');
            if (that.siblings("[data-for].display").not("[data-for='NaN']").length === 0)
                that.siblings("[data-for='NaN']").addClass('display');
        }
    }

    function resetPemisos() {
        $(".access-panel .la").removeClass('display');
        $(".access-panel .la").not('.la-lock').removeClass('partial');
        $(".access-panel [data-for]").val(false);
    }

    function loadMenus(container, menuContainer, size, callback) {
        var d = new Date();
        d = '?v=' + d.getTime();

        $.ajax({
            url: '/admin/permission',
            dataType: 'json',
            error: eHandler
        }).then(function(e) {
            var t = $.Deferred();
            var p = $.Deferred();
            $.get('/Mviews/roles_menu_base.html' + d, function(template) {
                t.resolve(template);
            });
            $.get('/Mviews/roles_menu_partial.html' + d, function(partial) {
                p.resolve(partial);
            });
            $.when(t, p).then(function(template, partial) {
                var i = 0;
                var rendered = Mustache.render(template, {
                    childs: e,
                    hasChilds: function() {
                        return this.childs && this.childs.length > 0;
                    },
                    section: function() {
                        return this.type == 1;
                    },
                    size: function() {
                        return size == undefined ? 'col-lg-4' : size
                    },
                    newIndex: function() {
                        return i++;
                    },
                    index: function() {
                        return i;
                    }
                }, {
                    child: partial
                });
                menuContainer.html(rendered);
                setEvents($(container));
            }).then(callback);
        });
    }

    function getRoles() {
        //Definición de Tabla
        var Kcolumns = [{
                data: 'id',
                title: '#'
            }, {
                data: 'name',
                title: 'Rol'
            }, {
                data: 'description',
                title: 'Descripcion'
            }, {
                data: 'created_at',
                title: 'Creado',
                render: dateFormaterTemplate
            }, {
                data: 'updated_at',
                title: 'Actualizado',
                render: dateFormaterTemplate
            }, {
                data: 'permissions',
                title: 'Actualizado',
                render: function(r) {
                    var render = "";
                    $.each(r, function(i, v) {
                        if (i != 0)
                            render += " - ";
                        title = "";
                        for (var key in v.pivot) {
                            switch (key) {
                                case "C":
                                    if (v.pivot[key] === "1")
                                        title += "<i class='la la-plus'></i>";
                                    break;
                                case "R":
                                    if (v.pivot[key] === "1")
                                        title += "<i class='la la-eye'></i>";
                                    break;
                                case "U":
                                    if (v.pivot[key] === "1")
                                        title += "<i class='la la-edit'></i>";
                                    break;
                                case "D":
                                    if (v.pivot[key] === "1")
                                        title += "<i class='la la-times'></i>";
                                    break;
                                default:
                                    break;
                            }
                        }
                        render += "<a href='javascript:void(0);' data-html='true' class='m-link' title=\"" + title + "\">" + v.title + "</a>";
                    });
                    return render;
                }
            },
            {
                data: 'p',
                title: 'Acciones',
                render: renderActions
            }
        ];
        var table = dataTable($('#dt'), '/admin/' + mod, Kcolumns);
        table.on('draw', function() {
            $('#dt').find('a').tooltip();
        });
        $("#dt").on('click', '.bDelete', deleteRol);
        $("#dt").on('click', '.bEdit', edt);
    }

    function deleteRol() {
        let delid = $(this).data('id');
        swal({
            title: "¿Seguro que deseas eliminar este registro?",
            text: "No habrá manera de revertir esta acción",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Si, Elimínalo",
            cancelButtonText: "Cancelar",
            confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
            cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
        }).
        then(function(r) {
            if (r.value) {
                $.ajax({
                    url: '/admin/' + mod + '/' + delid,
                    method: 'POST',
                    data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
                    dataType: 'json',
                    success: function(e) {
                        ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                        dt.ajax.reload();
                    },
                    error: eHandler
                });
            }
        });
    }

    function nuevoRol() {
        var form = $('#submitForm');
        form.ajaxSubmit({
            url: '/admin/' + mod,
            method: 'POST',
            success: function(response, status, xhr, $form) {
                ShowNotification('Rol registrado con exito.', 'success');
                resetPemisos();
                form[0].reset();
                dt.ajax.reload();
            },
            error: eHandler
        });
    }

    function edt() {
        let edid = $(this).data('id');
        let update = function() {
            $('#eModal #submitForm').validate({
                errorPlacement: ePlacement,
                errorClass: eClass,
                submitHandler(frm) {
                    var form = $('#eModal #submitForm');
                    form.ajaxSubmit({
                        url: '/admin/' + mod + '/' + edid,
                        method: 'PUT',
                        data: { _method: 'PUT' },
                        success: function(response, status, xhr, $form) {
                            ShowNotification('Actualización Exitosa', 'success');
                            dt.ajax.reload();
                            $("#eModal").modal('toggle');
                        },
                        error: eHandler
                    });
                }
            });
        }

        $.ajax({
            url: '/admin/' + mod + '/' + edid + '/edit',
            dataType: 'json',
            success: function(e) {
                $("#modalContainer").html(e.view);
                $.ajax({
                    url: '/admin/' + mod + '/' + edid,
                    method: 'GET',
                    data: { _token: $("[name='_token']").val() },
                    dataType: 'json',
                    error: eHandler
                }).then(function(e) {
                    loadMenus('#eModal', $('#edit_menus'), 'col-lg-6', function() {
                        $.each(e, function(i, v) {
                            var refInput = $("#modalContainer #submitForm").find("[name^='permisos['][name$='][id]'][value='" + parseInt(v.id) + "']");
                            var panel = refInput.parent().siblings('.access-panel');
                            refInput.siblings('input[type="checkbox"]').prop('checked', true);
                            for (const key in v.pivot) {
                                panel.find("#" + key).val(v.pivot[key] == "1");
                            }
                        });
                        $('#eModal #submitForm .access-panel [name*="permisos"]').each(updatePermisosUI);
                        $("#eModal").modal();
                        update();
                    });
                });

            },
            error: eHandler
        });

    }

    return {
        init: function() {
            var rules = {
                rol: {
                    required: true
                }
            };
            $('#submitForm').validate({
                errorPlacement: ePlacement,
                errorClass: eClass,
                rules: rules,
                submitHandler: nuevoRol
            });
            loadMenus('.m-content', $("#menus"));
            getRoles();
        }
    };
}();

$(document).ready(function() {
    Roles.init();
});