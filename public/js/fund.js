//Inicializacion
var mod = 'fund';
$(document).ready(function() {
    dataTable($('#dt'), '/admin/' + mod, Kcolumns);
    //drop1();
    validations();
    $('#dt').on('click', '.bActive', function() {});
    $('#dt').on('click', '.bEdit', edt);
    $('#dt').on('click', '.bDelete', del);
});

//Definición de Tabla
var Kcolumns = [{
        data: 'id',
        title: '#'
    }, {
        data: 'name',
        title: 'Nombre'
    }, {
        data: 'times_year',
        title: 'Pagos Especiales al Año'
    }, {
        data: 'created_at',
        title: 'Creado',
        render: dateFormaterTemplate
    }, {
        data: 'updated_at',
        title: 'Actualizado',
        render: dateFormaterTemplate
    },
    {
        data: 'p',
        title: 'Acciones',
        render: renderActions
    }
];

//Getters

function drop1() {
    $.ajax({
        url: '/admin/role',
        dataType: 'json',
        success: function(e) {
            $('#role').select2({
                placeholder: 'Seleccione un Rol de Usuario',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function validations() {
    $('#new').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#new');
            form.ajaxSubmit({
                url: '/admin/' + mod,
                method: 'POST',
                success: function(response, status, xhr, $form) {
                    ShowNotification('Registro Exitoso', 'success');
                    dt.ajax.reload();
                    form[0].reset();
                },
                error: eHandler
            });
        }
    });
}

//Edición

var edid;

function edt() {
    edid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $(".sSpecial").select2({ placeholder: 'Seleccione un Rol de Usuario', width: '100%' });
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    dt.ajax.reload();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}

//Eliminación

var delid;

function del() {
    delid = $(this).data('id');
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "No habrá manera de revertir esta acción",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Si, Elimínalo",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
        cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
    }).
    then(delHandler);
}

function delHandler(r) {
    if (r.value) {
        $.ajax({
            url: '/admin/' + mod + '/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                dt.ajax.reload();
            },
            error: eHandler
        });
    }
}