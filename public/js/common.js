function isActiveBadgeTemplate(row) {
    var status = { 'title': 'Inactivo', 'class': 'm-badge--danger' };
    if (row)
        status = { 'title': 'Activo', 'class': ' m-badge--success' };
    return '<span class="m-badge ' + status.class + ' m-badge--wide">' + status.title + '</span>';

}
$(document).ajaxComplete(function() {
    unBlockPage();
});

$(document).ajaxStart(function() {
    blockPage();
});

function dateFormaterTemplate(d) {
    return moment(d).format('DD-MM-YY HH:mm');
}

function blockPage() {
    mApp.blockPage({
        overlayColor: '#000000',
        type: 'loader',
        state: 'success',
        size: 'lg'
    });
}

function unBlockPage() {
    mApp.unblockPage();
}

var dt;

function dataTable(tbl, route, columns) {
    dt = tbl.DataTable({
        language: {
            processing: "Procesando...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ ",
            info: "Mostrando de _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "No hay datos disponibles",
            infoFiltered: "(Filtrando _MAX_ registros)",
            infoPostFix: "",
            loadingRecords: "Obteniendo Datos...",
            zeroRecords: "No hay datos disponibles para su búsqueda",
            emptyTable: "No hay datos disponibles",
            aria: {
                sortAscending: ": Ordenar Ascendente",
                sortDescending: ": Ordenar Descendente"
            }
        },
        ajax: route,
        serverSide: true,
        processing: true,
        destroy: true,
        responsive: true,
        dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>\n\t\t\t<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        buttons: [
            { extend: "print", text: "Imprimir" },
            { extend: "copyHtml5", text: "Copiar" },
            "excelHtml5",
            "pdfHtml5"
        ],
        columns: columns,
        drawCallback: function() {
            $(".ttip").tooltip();
        }
    });

    return dt;
}

jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requerido.",
    email: "Por favor ingrese un correo valido.",
    url: "Por favor ingrese un URL valido.",
    date: "Por favor ingrese una fecha valida.",
    number: "Por favor ingrese un numero valido.",
    digits: "Solo se permiten numeros en este campo.",
    equalTo: "Los valores ingresados no coinciden.",
    maxlength: jQuery.validator.format("Por favor ingrese menos de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor ingrese al menos {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor ingrese un valor de entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor ingrese un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor ingrese un valor igual o menos a {0}."),
    min: jQuery.validator.format("Por favor ingrese un valor igual o mayor a {0}.")
});

function ePlacement(error, elem) {
    elem.parent().parent().append(error);
}

function eHandler(xhr) {
    ShowNotification(xhr.responseJSON.message, 'error');
}

var eClass = 'm--font-danger';

function ShowNotification(message, type) {
    swal({
        position: "top-right",
        type: type,
        title: message,
        showConfirmButton: !1,
        timer: 1500
    });
}

function renderActions(r, s, d, u) {
    var t = '';
    if (r.a) {
        t += '<a data-container="body" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "Activar/Desactivar" class="bActive ttip m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon-only m-btn--pill">' +
            '<i class="la la-magic"></i></a>'
    }
    if (r.e) {
        t += '<a data-container="body" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "Editar" class="bEdit ttip m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon-only m-btn--pill">' +
            '<i class="la la-edit"></i></a>';
    }
    if (r.d) {
        t += '<a data-container="body" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "Eliminar" class="bDelete ttip m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon-only m-btn--pill">' +
            '<i class="la la-remove"></i></a>'
    }
    return t;
}