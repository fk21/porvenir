//Inicializacion
var mod = 'user';
$(document).ready(function() {
    //dataTable($('#dt'), '/admin/' + mod, Kcolumns);
    drop1();
    drop2();
    drop3();
    $('#fin').select2({
        placeholder: 'Financiamiento',
    });
    validations();
    $('#dt').on('click', '.bActive', function() {});
    $('#dt').on('click', '.bEdit', edt);
    $('#dt').on('click', '.bDelete', del);
});

//Definición de Tabla
var Kcolumns = [{
        data: 'pago',
        title: '# de Pago'
    }, {
        data: 'monto',
        title: 'Monto a Pagar',
        render(row) {
            return parseFloat(row).toLocaleString('en-US', {
                style: 'currency',
                currency: 'USD',
            });
        }
    },
    {
        data: 'tipo',
        title: 'Tipo de Pago',
        render(row) {
            var type = row == 0 ? 'primary' : row == 1 ? 'info' : 'warning'
            var txt = row == 0 ? 'Enganche' : row == 1 ? 'Pago Regular' : 'Pago Especial'
            return '<span class="m-badge m-badge--' + type + ' m-badge--wide">' + txt + '</span>';
        }
    },
    {
        data: 'fecha',
        title: 'Fecha de Pago'
    },
    {
        data: 'restante',
        title: 'Monto Restante',
        render(row) {
            return row.toLocaleString('en-US', {
                style: 'currency',
                currency: 'USD',
            });
        }
    }
];

//Getters

function drop1() {
    $.ajax({
        url: '/admin/client',
        dataType: 'json',
        success: function(e) {
            $('#client').select2({
                placeholder: 'Seleccione un Cliente',
                data: $.map(e.data, function(e) {
                    e.text = e.first_name + ' ' + e.middle_name + ' ' + e.last_name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function drop2() {
    $.ajax({
        url: '/admin/terrain',
        dataType: 'json',
        success: function(e) {
            $('#lot').select2({
                placeholder: 'Seleccione Lote',
                data: $.map(e.data, function(e) {
                    e.text = 'Lote: ' + e.lot + ' Manzana: ' + e.block;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function drop3() {
    $.ajax({
        url: '/admin/fund',
        dataType: 'json',
        success: function(e) {
            $('#fund').select2({
                placeholder: 'Seleccione pago especial',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    e.id = e.times_year;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function validations() {
    $('#new').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            getCorrida();
        }
    });
}

//Edición

var edid;

function edt() {
    edid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $(".sSpecial").select2({ placeholder: 'Seleccione un Rol de Usuario', width: '100%' });
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    dt.ajax.reload();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}

//Eliminación

var delid;

function del() {
    delid = $(this).data('id');
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "No habrá manera de revertir esta acción",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Si, Elimínalo",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
        cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
    }).
    then(delHandler);
}

function delHandler(r) {
    if (r.value) {
        $.ajax({
            url: '/admin/' + mod + '/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                dt.ajax.reload();
            },
            error: eHandler
        });
    }
}
var corrida = [];

function getCorrida() {

    var fin = $("#fin").val();
    var enganche = $("#enganche").val();
    var opcionesA = parseInt($("#fund").val()) != 0 ? 12 / parseInt($("#fund").val()) : 0;
    var monto = $("#monto").val();
    var fecha = moment($("#fecha").val(), 'DD/MM/YYYY');
    var precioLote = $("#price").val();

    var montoEnganche = precioLote * (enganche / 100);
    var kPagosEspeciales = opcionesA > 0 ? fin / opcionesA : 0;
    var montoDivisible = (precioLote - montoEnganche) - (kPagosEspeciales * monto);
    var montoMensual = montoDivisible / (fin - kPagosEspeciales);
    var montoRestante = precioLote - montoEnganche;
    corrida.splice(0, corrida.length);
    corrida.push({
        pago: 0,
        monto: montoEnganche,
        tipo: 0,
        fecha: moment(new Date()).format('DD-MM-YYYY'),
        restante: montoRestante
    });

    for (var i = 1; i <= fin; i++) {
        var mnt = montoMensual;
        var tipo = 1; //pago mensual
        if (opcionesA != 0) {
            mnt = i % opcionesA != 0 ? montoMensual : monto;
            tipo = i % opcionesA != 0 ? 1 : 2;
        }
        montoRestante -= mnt;
        corrida.push({
            pago: i,
            monto: mnt,
            tipo: tipo,
            fecha: fecha.format('DD-MM-YYYY'),
            restante: montoRestante
        });
        fecha = fecha.add(1, 'months');

    }
    dataTableLocal($("#dt"), corrida, Kcolumns);
}

function dataTableLocal(tbl, dt, columns) {
    dt = tbl.DataTable({
        language: {
            processing: "Procesando...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ ",
            info: "Mostrando de _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "No hay datos disponibles",
            infoFiltered: "(Filtrando _MAX_ registros)",
            infoPostFix: "",
            loadingRecords: "Obteniendo Datos...",
            zeroRecords: "No hay datos disponibles para su búsqueda",
            emptyTable: "No hay datos disponibles",
            aria: {
                sortAscending: ": Ordenar Ascendente",
                sortDescending: ": Ordenar Descendente"
            }
        },
        data: dt,
        destroy: true,
        responsive: true,
        dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>\n\t\t\t<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        buttons: [
            { extend: "print", text: "Imprimir" },
            { extend: "copyHtml5", text: "Copiar" },
            "excelHtml5",
            "pdfHtml5"
        ],
        columns: columns,
        drawCallback: function() {
            $(".ttip").tooltip();
        }
    });

    return dt;
}