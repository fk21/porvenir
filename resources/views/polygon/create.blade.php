@extends('layouts.admin')
@section('scripts')
<script src="/js/polygon.js"></script>
@stop
@section('title')
Polígonos
@stop
@section('content')
@if($crud->c)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-user"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Nuevo Punto
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <form action="" id="new">
                <div class="m-portlet__body">
                    
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <select name="terrain_id" class="form-control m-select2" required id="terrain">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="number" class="form-control m-input" required  placeholder="Orden" name="order">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-sort-numeric-asc"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="number" class=" form-control form-control-danger m-input" required placeholder="Latitud" name="lat">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-map-marker"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="number" class="form-control m-input" required  placeholder="Longitud" name="lng">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-map-marker"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-info m-btn--pill m-btn--air">Registrar</button>
                        <button type="reset" class="btn btn-outline-danger m-btn--pill m-btn--air">Cancelar</button>
                        
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@if($crud->r)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-users"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Puntos Registrados
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <table id="dt" class="table table-striped- table-bordered table-hover table-checkable">
                    
                </table>
                <div class="m_datatable">
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div id="modalContainer">
    
</div>

@stop