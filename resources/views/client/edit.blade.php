<!--begin::Modal-->
<div class="modal fade" id="eModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Editar Registro
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="editForm">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="text" value="{{$client->first_name}}" class=" form-control form-control-danger m-input" required placeholder="Nombre" name="first_name">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-user"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="text" value="{{$client->middle_name}}" class=" form-control form-control-danger m-input" required placeholder="Apellido Paterno" name="middle_name">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-user"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span></span>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="text" value="{{$client->last_name}}" class=" form-control form-control-danger m-input" required placeholder="Apellido Materno" name="last_name">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-user"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="text" value="{{$client->cellphone}}" class=" form-control form-control-danger m-input" minlength="10"  placeholder="Celular" name="cellphone">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-mobile-phone"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span></span>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="text" value="{{$client->phone}}" class=" form-control form-control-danger m-input"  placeholder="Teléfono Casa" name="phone">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-phone"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="text" value="{{$client->street}}" class=" form-control form-control-danger m-input"  required placeholder="Calle" name="street">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-home"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span></span>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="text" value="{{$client->suburb}}" class=" form-control form-control-danger m-input" required placeholder="Colonia" name="suburb">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-home"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    
                                    <input type="text" value="{{$client->city}}" class=" form-control form-control-danger m-input"  required placeholder="Ciudad" name="city">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-home"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success m-btn--pill m-btn--air">
                        Editar Registro
                    </button>
                    <button type="button" class="btn btn-outline-danger m-btn--pill m-btn--air" data-dismiss="modal">
                            Cancelar
                        </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
