@extends('layouts.admin')
@section('scripts')
<link href="/css/permission.css" rel="stylesheet" type="text/css" />
<script src="/assets/vendors/custom/jquery-ui/jquery-ui.bundle.js"></script>
<script src="/assets/vendors/custom/nested-sortable/jquery.mjs.nestedSortable.js"></script>
<script src="/assets/vendors/custom/mustache/mustache.js"></script>
<script src="/js/permission.js"></script>
@stop
@section('content')
<div class="row" id="menusApp">
    <div class="col-sm-6">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-add"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Nuevo Menú
                        </h3>
                    </div>
                </div>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right" id="new">
                @csrf
                <div class="m-portlet__body">
                    <div class="m-form__group form-group">
                        <label>
                            Menu
                        </label>
                        <div class="m-radio-inline">
                            <label class="m-radio">
                                <input type="radio" name="menu" id="type_menu" value="1" checked>
                                Si
                                <span></span>
                            </label>
                            <label class="m-radio">
                                <input type="radio" name="menu" value="0">
                                No
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="title">
                            Nombre del Menú
                        </label>
                        <input type="text" class="form-control m-input" id="title" name="title" placeholder="Usuarios">
                    </div>
                    <div class="form-group m-form__group ">
                        <label for="route">
                            Ruta
                        </label>
                        <input type="text" class="form-control m-input " id="route" name="route" placeholder="App ">
                    </div>
                    <div class="form-group m-form__group ">
                        <label for="icon">
                            Icóno
                        </label>
                        <select class="form-control m-select2 " id="icon" name="icon" data-placeholder="flaticon-users ">        
                        </select>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit ">
                    <div class="m-form__actions m-form__actions ">
                        <div class="row ">
                            <div class="col-sm-12 ">
                                <button type="submit" id="save" class="btn btn-primary pull-right " style="margin-left: 15px; ">
                                    Guardar
                                </button>
                                <button type="reset" class="btn btn-secondary pull-right ">
                                    Cancelar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-6 ">
        <div class="m-portlet ">
            <div class="m-portlet__head ">
                <div class="m-portlet__head-caption ">
                    <div class="m-portlet__head-title ">
                        <span class="m-portlet__head-icon ">
                            <i class="flaticon-list-1 "></i>
                        </span>
                        <h3 class="m-portlet__head-text ">
                            Constructor de Menus
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body ">
                <ol class="sortable" id="menu_builder">
                </ol>
            </div>
        </div>
    </div>
</div>
<div id="modalContainer">

</div>

@stop